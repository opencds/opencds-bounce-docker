# Version: 0.0.1

FROM ubuntu:16.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# Install packages
ENV REFRESHED_AT 2018-01-18
RUN apt-get update && \
    apt-get install -yq --no-install-recommends wget unzip tar pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Install Java
COPY files/java/jdk-8u151-linux-x64.tar.gz /
RUN mkdir /usr/local/java
RUN tar -zxf jdk-8u151-linux-x64.tar.gz -C /usr/local/java
RUN rm -f jdk-8u151-linux-x64.tar.gz

ENV JAVA_HOME /usr/local/java/jdk1.8.0_151
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Install TOMCAT
ENV TOMCAT_MAJOR_VERSION 9
ENV TOMCAT_MINOR_VERSION 9.0.0.M9
ENV CATALINA_HOME /usr/local/tomcat

RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
    wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
    tar zxf apache-tomcat-*.tar.gz && \
    rm apache-tomcat-*.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

COPY files/tomcat/tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml
COPY files/tomcat/setenv.sh ${CATALINA_HOME}/bin/setenv.sh
# Allow remote access to tomcat manager
COPY files/tomcat/manager.xml ${CATALINA_HOME}/conf/Catalina/localhost/manager.xml

# Set context.xml
COPY files/tomcat/context.xml ${CATALINA_HOME}/conf/context.xml

# Add .keystore
COPY files/tomcat/.keystore ${CATALINA_HOME}/.keystore

# Configure https
RUN sed -i "s#</Server>##g" ${CATALINA_HOME}/conf/server.xml; \
	sed -i "s#  </Service>##g" ${CATALINA_HOME}/conf/server.xml; \
	echo '    <Connector port="8443" protocol="HTTP/1.1" SSLEnabled="true" maxThreads="150" scheme="https" secure="true" clientAuth="false" sslProtocol="TLS" keystoreFile=".keystore" keystorePass="opencds" />' >> ${CATALINA_HOME}/conf/server.xml; \
	echo '  </Service>' >> ${CATALINA_HOME}/conf/server.xml; \
	echo '</Server>' >> ${CATALINA_HOME}/conf/server.xml

# Install OpenCDS
COPY files/opencds/opencds.properties /root/.opencds/opencds.properties
COPY files/opencds/sec.xml /root/.opencds/sec.xml
# Copy knowledge repository
COPY files/opencds/opencds-knowledge-repository-data.zip / 
RUN unzip opencds-knowledge-repository-data.zip -d /root/.opencds/
RUN rm opencds-knowledge-repository-data.zip

# Copy knowledgeModules.xml
COPY files/opencds/knowledgeModules.xml /root/.opencds/opencds-knowledge-repository-data/resources_v1.3/

# Copy semanticSignifiers.xml
COPY files/opencds/semanticSignifiers.xml /root/.opencds/opencds-knowledge-repository-data/resources_v1.3/

# Copy opencds-plugins.xml
COPY files/opencds/opencds-plugins.xml /root/.opencds/opencds-knowledge-repository-data/resources_v1.3/plugins/

# Copy opencds war
COPY files/opencds/opencds-decision-support-service.war /
RUN unzip opencds-decision-support-service.war -d ${CATALINA_HOME}/webapps/opencds-decision-support-service
RUN rm -f opencds-decision-support-service.war

# Add bounce knowledge package
COPY files/bounce-kb/opencds-bounce-1.0-jar-with-dependencies.jar /root/.opencds/opencds-knowledge-repository-data/resources_v1.3/knowledgePackages/org.opencds^opencds-bounce^1.0.jar

# Add image scripts
COPY files/img_scripts/run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh

# Add VOLUMEs to allow backup of config and databases
VOLUME ["/root/.opencds", "/usr/local/tomcat/webapps"]

EXPOSE 8080
EXPOSE 8443

CMD ["/usr/local/bin/run.sh"]

