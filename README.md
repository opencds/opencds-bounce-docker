# Build image

`docker build -t="opencds/opencds-bounce-cds-hooks" .`

# Create container (examples)

`docker run -d -p 8082:8080 --name opencds-bounce-cds-hooks opencds/opencds-bounce-cds-hooks`

`docker run -d -p 8082:8080 --restart always --name opencds-bounce-cds-hooks opencds/opencds-bounce-cds-hooks`
# Interactive mode

`docker run -it -p 8082:8080 -p 8443:8443 -p 1043:1043 --name opencds-bounce-cds-hooks opencds/opencds-bounce-cds-hooks /bin/bash`

# Verify that the service is available 

`http://localhost:8082/opencds-decision-support-service/`

