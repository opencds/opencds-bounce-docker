#!/bin/bash

java \
  -XX:+UnlockExperimentalVMOptions \
  -XX:+UseCGroupMemoryLimitForHeap \
  -XX:MaxRAMFraction=1 -XshowSettings:vm -version

# start tomcat
sh ${CATALINA_HOME}/bin/catalina.sh run

#sh ${CATALINA_HOME}/bin/catalina.sh jpda start
#tail -f /usr/local/tomcat/logs/catalina.out
